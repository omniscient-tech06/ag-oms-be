'use strict'

import Express from 'express'
import AdminPanelUserResource from '@ot06/rsrc-admin-panel-user-mongo'

const routesConfig = {
  routes: {
    login: { enabled: true },

    createOne: { enabled: true },
    createMany: { enabled: true },

    findOne: { enabled: true },
    findMany: { enabled: true },
    findOneBy: { enabled: true },
    findManyBy: { enabled: true },
    findById: { enabled: true },

    updateOne: { enabled: true },
    updateMany: { enabled: true },
    updateOneBy: { enabled: true },
    updateManyBy: { enabled: true },
    updateById: { enabled: true },

    remove: { enabled: true },
    removeById: { enabled: true },

    list: { enabled: true },
    search: { enabled: true },
    findByDate: { enabled: true },
    findByDateRange: { enabled: false },

    aggregate: { enabled: false }
  }
}

const AdminPanelUserRouter = new Express.Router()
const AdminPanelUser = new AdminPanelUserResource(AdminPanelUserRouter, routesConfig)

export default AdminPanelUser
