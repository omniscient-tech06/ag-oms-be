'use strict'

import Express from 'express'
import VersionResource from '@ot06/rsrc-version'

const routesConfig = {
  routes: {
    get: { enabled: true }
  }
}

const VersionRouter = new Express.Router()
const Version = new VersionResource(VersionRouter, routesConfig)

export default Version
