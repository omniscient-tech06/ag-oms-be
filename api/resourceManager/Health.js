'use strict'

import Express from 'express'
import HealthResource from '@ot06/rsrc-health'

const routesConfig = {
  routes: {
    getStatus: { enabled: true }
  }
}

const HealthRouter = new Express.Router()
const Health = new HealthResource(HealthRouter, routesConfig)

export default Health
