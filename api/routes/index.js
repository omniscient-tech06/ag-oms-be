'use strict'

import { expressMiddlewares } from '@ot06/be-utils'
import {
  Health,
  Version,

  AdminPanelUser
} from '../resourceManager'

const { ResponseHandler } = expressMiddlewares
const responseHandler = new ResponseHandler()

const Routes = [
  { path: '/health', router: Health.Router },
  { path: '/version', router: Version.Router },

  { path: '/admin-panel-users', router: AdminPanelUser.Router }
]

Routes.init = (app) => {
  if (!app || !app.use) {
    console.error('[Error] Route Initialization Failed: app / app.use is undefined')
    return process.exit(1)
  }

  app.enable('strict routing')

  // Custom Routes
  Routes.forEach(route => app.use(route.path, route.router))

  // Final Route Pipeline
  app.use('*', responseHandler.handleResponse)

  // Route Error Handler
  app.use(responseHandler.handleError)
}

export default Routes
