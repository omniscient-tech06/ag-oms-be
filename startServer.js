'use strict'

import { SERVER_CONFIG, EXPS_CONFIG } from './config'

const { PORT } = SERVER_CONFIG

const startServer = async (app) => {
  try {
    // Start Listening on Configured Port
    console.log(`[Info] Service: ${EXPS_CONFIG.SERVICE_NAME}. Starting Server on Port: ${PORT}`)
    await app.listen(PORT)
    console.log('[Info] Server Started Successfully')
  } catch (error) {
    console.log(error)
    process.exit(1)
  }
}

export default startServer
